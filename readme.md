# MacOS workstation setup script

A shell script to setup a required minimum of basic tools I use in my daily activities. Inspired by [laptop](https://github.com/thoughtbot/laptop) script by Thoughtbot.

## Compatibility

- macOS 10.13.5

## Installation

Clone repository, run script. You can run script multiple times if needed.

    bash mac.sh
